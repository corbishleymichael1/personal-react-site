import './navbar.css'
import { Link } from 'react-router-dom';

export function Navbar() {
    return (
        <nav className='site-nav'>
            <Link to="/personal-react-site/" className="site-title">Michael Corbishley</Link>
        </nav>
    );
}


